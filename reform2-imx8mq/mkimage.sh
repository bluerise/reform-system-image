#!/bin/bash

set -x
set -e

# make sure build tools are installed
# tested with bare Ubuntu 20.04 server
sudo apt-get -y install parted multistrap udisks2 gcc-aarch64-linux-gnu make device-tree-compiler qemu-user-static binfmt-support build-essential bison flex libssl-dev mmdebstrap

# build the linux kernel
./mkkernel.sh
# build u-boot
./mkuboot.sh

# build the debian userland and configure it
sudo ./mkuserland.sh
# chroot into the userland and build custom packages (mesa, xserver...)
sudo ./mkuserland2.sh

# Rescue System ---------------------------------------------------------

SIZE=4096M
# create ext4 partition from target root directory directly at 4MiB offset
sudo /sbin/mke2fs -v -L 'MNTRESCUE' -N 0 -O 64bit -E offset=4194304 -d target-userland -m 5 -r 1 -t ext4 reform-rescue-system.img $SIZE

sudo /sbin/parted -s reform-rescue-system.img "mklabel msdos"
sudo /sbin/parted -s reform-rescue-system.img "mkpart primary ext4 4MiB -1s"
sudo /sbin/parted -s reform-rescue-system.img print

# mkuboot.sh needs to run before. this creates flash.bin.
# install u-boot for i.MX8MQ
sudo dd if=./u-boot/flash.bin of=reform-rescue-system.img conv=notrunc bs=1k seek=33

echo Reform Rescue System Image created: reform-rescue-system.img

# Full System -----------------------------------------------------------

# chroot into the userland and add extra applications
sudo ./mkuserland3.sh

SIZE=9000M
# create ext4 partition from target root directory directly at 4MiB offset
sudo /sbin/mke2fs -v -L 'MNTREFORM' -N 0 -O 64bit -E offset=4194304 -d target-userland -m 5 -r 1 -t ext4 reform-system.img $SIZE

sudo /sbin/parted -s reform-system.img "mklabel msdos"
sudo /sbin/parted -s reform-system.img "mkpart primary ext4 4MiB -1s"
sudo /sbin/parted -s reform-system.img print

# install u-boot for i.MX8MQ
sudo dd if=./u-boot/flash.bin of=reform-system.img conv=notrunc bs=1k seek=33

echo Reform Full System Image created: reform-system.img

