#!/bin/bash

chroot target-userland /bin/bash <<EOF
export LC_ALL=C
export LANGUAGE=C
export LANG=C

cd /root
mkdir -p src
cd src

git clone --depth 1 https://gitlab.freedesktop.org/mesa/drm.git
git clone --depth 1 --branch mesa-20.3.4 https://gitlab.freedesktop.org/mesa/mesa.git
git clone --depth 1 https://github.com/wayland-project/wayland.git
git clone https://github.com/swaywm/wlroots.git
git clone https://github.com/swaywm/sway.git
git clone --depth 1 https://gitlab.freedesktop.org/xorg/xserver.git
git clone --depth 1 https://github.com/Alexays/Waybar.git
git clone --depth 1 https://github.com/any1/wayvnc.git
git clone --depth 1 https://github.com/Hjdskes/cage.git

cd drm
meson build -Detnaviv=true -Dradeon=false -Damdgpu=false -Dvmwgfx=false -Dfreedreno=false -Dvc4=false -Dnouveau=false
ninja -C build install
cd ..

ldconfig

cd mesa
meson build -Dplatforms=x11,wayland -Ddri3=true -Dgallium-drivers=swrast,etnaviv,kmsro,virgl -Dgbm=enabled -Degl=enabled -Dbuildtype=release -Db_ndebug=true

# patch that disables broken MSAA in etnaviv (not yet upstreamed for some reason)
patch -p1 <<ENDPATCH
--- a/src/gallium/drivers/etnaviv/etnaviv_screen.c
+++ b/src/gallium/drivers/etnaviv/etnaviv_screen.c
@@ -445,6 +445,10 @@ etna_screen_is_format_supported(struct pipe_screen *pscreen,
    struct etna_screen *screen = etna_screen(pscreen);
    unsigned allowed = 0;

+   /* HACK to disable all MSAA, as is causes GPU crashes */
+   if (sample_count > 1)
+      return false;
+
    if (!gpu_supports_texture_target(screen, target))
       return false;
ENDPATCH

# apply other mesa patches
patch -p1 <../patches-mesa/7603.patch
#patch -p1 <../patches-mesa/8618.patch
#patch -p1 <../patches-mesa/9214.patch

ninja -C build install
cd ..

ldconfig

cd wayland
meson build -Ddocumentation=false
ninja -C build install
cd ..

ldconfig

cd wlroots
git checkout 0.12.0
meson build
ninja -C build install
cd ..

ldconfig

cd sway
git checkout 1.5.1
meson build
ninja -C build install
chmod +s /usr/local/bin/sway
cd ..

cd xserver

# patch to work around flickery GTK2 and other legacy X11 GUIs on etnaviv

patch -p1 <<ENDPATCH
diff --git a/glamor/glamor_render.c b/glamor/glamor_render.c
index be0741a..1dd2876 100644
--- a/glamor/glamor_render.c
+++ b/glamor/glamor_render.c
@@ -1584,6 +1584,8 @@ glamor_composite_clipped_region(CARD8 op,
     if (prect != rect)
         free(prect);
  out:
+    glFinish();
+
     if (temp_src != source)
         FreePicture(temp_src, 0);
     if (temp_mask != mask)
ENDPATCH

meson build -Dxorg=true -Dxwayland=true -Dglamor=true -Dxwayland_eglstream=false -Dxnest=false -Ddmx=false -Dxvfb=true -Dxwin=false -Dxephyr=false -Ddri3=true
ninja -C build install
cd ..

# overwrite /usr/bin/Xwayland with symlink to our Xwayland (FIXME: brittle)

rm -f /usr/bin/Xwayland
ln -s /usr/local/bin/Xwayland /usr/bin/Xwayland

cd Waybar
meson build
ninja -C build install
cd ..

cd wayvnc
mkdir subprojects
cd subprojects
git clone https://github.com/any1/neatvnc.git
git clone https://github.com/any1/aml.git
cd ..
meson build
ninja -C build install
cd ..

cd cage
meson build
ninja -C build install
cd ..

EOF
